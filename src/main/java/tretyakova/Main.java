package tretyakova;

public class Main {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[]{new Square(5), new Circle(9), new Paralellepiped(6,5)};
        for (Shape shape :
                shapes) {
            if (shape instanceof Paralellepiped){
                System.out.println(shape.getName() + " " + ((Paralellepiped) shape).getVolume());
            } else {
                System.out.println(shape.getName() + " " + shape.getArea());
            }
            
        }

    }
}

abstract class Shape {
    private String name;

    public Shape(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }

    double getArea() {
        return 0;
    }
}

class Square extends Shape {
    double side;

    protected Square(String name, double side) {
        super(name);
        this.side = side;
    }

    public Square(double side) {
        this("Square", side);
    }

    @Override
    double getArea() {
        return side * side;
    }
}

class Circle extends Shape {
    double radius;

    public Circle(double radius) {
        super("Circle");
        this.radius = radius;
    }

    @Override
    double getArea() {
        return Math.PI * radius * radius;
    }
}
class Paralellepiped extends Square {
    double height;

    public Paralellepiped(double side, double height) {
        super("Paralellepiped", side);
        this.height = height;
    }

    double getVolume(){
        return getArea()*height;
    }
}
